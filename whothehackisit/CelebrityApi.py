import logging
import os
import requests

class CelebrityApi:

    _apiRootUrl = 'https://api.api-ninjas.com/v1/celebrity'
    _apiKey = None

    "Initialize Celebrity API client"
    def __init__(self):
        apiKey = os.getenv('NINJAS_API_KEY')
        logging.debug('Initializing CelebrityApi with apiKey=%s', apiKey)
        self._apiKey = apiKey

    "Find all celebrities with given name"
    def getAllByName(self, name):
        logging.debug('Called CelebrityApi.getAllByName() with name=%s', name)
        response = requests.get(self._apiRootUrl, params={'name': name}, headers={'X-Api-Key': self._apiKey})
        return response.json()