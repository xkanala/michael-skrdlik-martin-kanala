import uuid
from disnake.ext import commands
import os
import logging
import whothehackisit.DiscordCmd as bot_cmd

TOKEN = os.getenv('DISCORD_TOKEN', '')
CHANNEL_NAME = os.getenv('DISCORD_CHANNEL', 'channel')
TMP_FOLDER = os.getenv('TMP_FOLDER', 'tmp/')
client = commands.Bot(command_prefix='!')

"A wrapper method for discord commands with implementation of common steps"
async def discord_command(ctx, cmd_callback):
    if ctx.message.channel.name == CHANNEL_NAME:
        await ctx.message.channel.send('Starting to process your command...')

        if len(ctx.message.attachments) == 0:
            await ctx.message.channel.send('No attachments detected!')
            return

        if ctx.message.attachments[0].filename.lower().endswith(('jpeg', 'jpg', 'png')) == False:
            await ctx.message.channel.send('Invalid file type. Allowed are only JPEG and PNG.')
            return

        try:
            filepath = os.path.join(os.getcwd(), TMP_FOLDER, str(uuid.uuid4()) + '-' + ctx.message.attachments[0].filename)
            await ctx.message.attachments[0].save(filepath)
        except Exception as e:
            logging.error('An error occurred during image processing.', e)
            await ctx.message.channel.send('An error occurred during image processing. Please try it again.')
        else:
            try:
                await cmd_callback(client, ctx, filepath)
            except Exception as e:
                logging.error('An error occurred during command processing.', e)
                await ctx.message.channel.send('An error occurred during command processing. Please try it again.')
            finally:
                os.remove(filepath)
        return

@client.event
async def on_ready():
    logging.info('Bot is ready. Successfully logged in as %s.', '{0.user}'.format(client))


@client.command()
async def how_many_faces(ctx):
    await discord_command(ctx, bot_cmd.how_many_faces)

@client.command()
async def who_is_it(ctx):
    await discord_command(ctx, bot_cmd.who_is_it)

@client.command()
async def add_person(ctx):
    await discord_command(ctx, bot_cmd.add_person)

def bot_start():
    client.run(TOKEN)
