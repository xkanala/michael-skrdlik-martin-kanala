import logging
import os
import pymongo

class PersonRepository:

    _client = None
    _db = None
    _collection = None

    "Initialize DB connection and specific collection"
    def __init__(self):
        connectionString = os.getenv('MONGO_CONNECTION_STRING')
        databaseName = os.getenv('MONGO_DATABASE_NAME')
        logging.debug('Initializing PersonRepository with connectionString=%s and databaseName=%s', connectionString, databaseName)
        self._client = pymongo.MongoClient(connectionString)
        self._db = self._client[databaseName]
        self._collection = self._db.persons

    "Save new person record"
    def save(self, person):
        logging.debug('Called PersonRepository.save() with person=%s', person)
        self._collection.insert_one(person)

    "Get all person(s) by given name"
    def findAllByName(self, name):
        logging.debug('Called PersonRepository.findAllByName() with name=%s', name)

    "Get one person by ObjectId"
    def findOneByObjectId(self, objectId):
        logging.debug('Called PersonRepository.getOneByObjectId() with objectId=%s', objectId)
        return self._collection.find_one(objectId)

    "Get all face encodings"
    def findAllFaceEncodings(self):
        logging.debug('Called PersonRepository.getAllFaceEncodings()')
        return self._collection.find({}, {'faceEncoding': 1})