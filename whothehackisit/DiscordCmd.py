import logging
import asyncio
import re
from whothehackisit.CelebrityApi import CelebrityApi
from whothehackisit.FaceRecognition import FaceRecognition
from whothehackisit.PersonRepository import PersonRepository

celebrityApi = CelebrityApi()
personRepository = PersonRepository()
faceRecognition = FaceRecognition()

"Concatenate a 'Person description' string"
def get_person_description_message(fullName, birthDay, nationality, gender):
    return 'Full name: ' + fullName + '\nDate of birth: ' + birthDay + '\nNationality: ' + nationality + '\nGender: ' + gender

"Wait for the user input, validate it and return given value"
async def get_user_input(client, ctx, propertyName, exampleValue, validationFce, allowEmpty):
    await ctx.message.channel.send('What is the ' + propertyName + ' of the person in the picture? (e.g. "' + exampleValue + '")')

    def check_author(m):
        return m.author == ctx.message.author and m.channel == ctx.message.channel

    try:
        message = await client.wait_for('message', check=check_author, timeout=30.0)

        value = None
        if validationFce(message.content) == True:
            value = message.content
        else:
            options = "yes/no/empty" if allowEmpty else 'yes/no'
            await ctx.message.channel.send('The ' + propertyName + ' seems odd. Is it really correct? (' + options + ')')

            try:
                reply = await client.wait_for('message', check=check_author, timeout=30.0)
            except asyncio.TimeoutError:
                return None
            else:
                if reply.content.lower() == 'yes' or reply.content.lower() == 'y':
                    value = message.content
                elif allowEmpty and reply.content.lower() == 'empty' or reply.content.lower() == 'e':
                    value = ''
                else:
                    value = await get_user_input(client, ctx, propertyName, exampleValue, validationFce, allowEmpty)
        return value
    except asyncio.TimeoutError:
        return None

"Shallow get of dict property, returns empty string when property does not exist"
def dict_property_shallow_get(obj, key):
    try:
        return obj[key]
    except KeyError:
        return ''

"Construct a Person entity from given fullName, faceEncodings and requested additional information"
async def prepare_person_record(client, ctx, fullName, faceEncodings):
    birthDate = ''
    gender = ''
    nationality = ''
    celebrity = celebrityApi.getAllByName(fullName)

    index = 0
    if len(celebrity) == 0:
        await ctx.message.channel.send('No celebrity found with this name. Please provide additional information by manually.')
    else:
        await ctx.message.channel.send('Please find below list of found celebrities with the given name. Please reply a number of the correct person or 0 for manual mode.')

        for i in range(len(celebrity)):
            await ctx.message.channel.send('[' + str(i + 1) + ']:\n'
                + get_person_description_message(celebrity[i]['name'],
                    dict_property_shallow_get(celebrity[i], 'birthdy'),
                    dict_property_shallow_get(celebrity[i], 'nationality'),
                    dict_property_shallow_get(celebrity[i], 'gender'))
            )

        def validation_number(value):
            return str(value).isdigit()
        index = await get_user_input(client, ctx, 'number', '0/1/...', validation_number, False)
        if index == None:
            index = 0
            await ctx.message.channel.send('You did not provide a number of the person. Switching to the manual mode.')
        else:
            index = int(index)

    if index > 0:
        birthDate = dict_property_shallow_get(celebrity[i], 'birthdy')
        gender = dict_property_shallow_get(celebrity[i], 'gender')
        nationality = dict_property_shallow_get(celebrity[i], 'nationality')

    if birthDate == '':
        def validation_birthDate(value):
            # todo
            return True
        birthDate = await get_user_input(client, ctx, 'birth date', '1970-01-01', validation_birthDate, True)
        if birthDate == None:
            birthDate = ''
            await ctx.message.channel.send('You did not provide a birth date of the person. Use empty string instead.')

    if gender == '':
        def validation_gender(value):
            # todo
            return True
        gender = await get_user_input(client, ctx, 'gender', 'male', validation_gender, True)
        if gender == None:
            gender = ''
            await ctx.message.channel.send('You did not provide a gender of the person. Use empty string instead.')

    if nationality == '':
        def validation_nationality(value):
            # todo
            return True
        nationality = await get_user_input(client, ctx, 'nationality', 'us', validation_nationality, True)
        if nationality == None:
            nationality = ''
            await ctx.message.channel.send('You did not provide a nationality of the person. Use empty string instead.')

    return {
        'fullName': fullName,
        'faceEncoding': faceRecognition.getFaceEncodingAsString(faceEncodings[0]),
        'birthDay': birthDate,
        'gender': gender,
        'nationality': nationality
    }

"A command implementation which returns the count of faces in the picture"
async def how_many_faces(client, ctx, filepath):
    logging.debug('Start processing how_many_faces command...')
    count = faceRecognition.getFacesCount(filepath)

    message = ''
    if count == 0:
        message = 'The uploaded image does not contain any face.'
    elif count == 1:
        message = 'The uploaded image contains 1 face.'
    else:
        message = 'The uploaded image contains ' + str(count) + ' faces.'

    await ctx.message.channel.send(message)

"A command implementation which returns an information about person in the picture"
async def who_is_it(client, ctx, filepath):
    logging.debug('Start processing who_is_it command...')
    knownPeople = list(personRepository.findAllFaceEncodings())

    recognizedPersonId = faceRecognition.identifyPerson(filepath, knownPeople)
    if recognizedPersonId != None:
        knownPerson = personRepository.findOneByObjectId(recognizedPersonId)

        await ctx.message.channel.send(get_person_description_message(knownPerson['fullName'], knownPerson['birthDay'], knownPerson['nationality'], knownPerson['gender']))
    else:
        await ctx.message.channel.send('No matching person found in our database.')

"A command implementation to augment the known people database with new record"
async def add_person(client, ctx, filepath):
    logging.debug('Start processing add_person command...')
    faceEncodings = faceRecognition.getFaceEncodings(filepath)
    if len(faceEncodings) > 1:
        await ctx.message.channel.send('The image must contain only one face.')
        return

    knownPeople = list(personRepository.findAllFaceEncodings())
    knownPersonId = faceRecognition.identifyPerson(filepath, knownPeople)
    if knownPersonId != None:
        knownPerson = personRepository.findOneByObjectId(knownPersonId)
        await ctx.message.channel.send('This person is already in our database.\n\n' + get_person_description_message(knownPerson['fullName'], knownPerson['birthDay'], knownPerson['nationality'], knownPerson['gender']))
        return

    def validation_fullName(value):
        return re.search('^([A-ž-\']+) ([A-ž-\']+)$', value) != None

    fullName = await get_user_input(client, ctx, 'full name', 'John Doe', validation_fullName, False)
    if fullName == None:
        await ctx.message.channel.send('You did not provide a name of the person. Please start again.')
        return

    newPerson = await prepare_person_record(client, ctx, fullName, faceEncodings)
    if newPerson != None:
        personRepository.save(newPerson)
        await ctx.message.channel.send(fullName + ' has been successfully added into the database.')

    return