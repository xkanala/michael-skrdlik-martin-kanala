import logging
import os
from dotenv import load_dotenv

load_dotenv()

loggingLevel = os.getenv('LOGGING_LEVEL', 'INFO')
logging.basicConfig(format='%(asctime)s %(levelname)s --- %(message)s', encoding='utf-8', level=loggingLevel)

logging.info('---- Who The Hack Is It Discord Bot is starting... ----')