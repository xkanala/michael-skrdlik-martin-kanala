import logging
import face_recognition
import numpy

class FaceRecognition:

    "Load image as numpy array by given file path"
    def loadImage(self, filePath):
        logging.debug('Called FaceRecognition.loadImage() with filePath=%s', filePath)
        return face_recognition.api.load_image_file(filePath)

    "Get face encodings for given image"
    def getFaceEncodings(self, filePath):
        logging.debug('Called FaceRecognition.getFaceEncodings() with filePath=%s', filePath)
        image = self.loadImage(filePath)
        return face_recognition.api.face_encodings(image, num_jitters=10, model='large')

    "Get string representation of given face encoding"
    def getFaceEncodingAsString(self, faceEncoding):
        logging.debug('Called FaceRecognition.getFaceEncodingAsString() with faceEncoding=%s', faceEncoding)
        return numpy.ndarray.tostring(faceEncoding)

    "Get count of faces on given image"
    def getFacesCount(self, filePath):
        logging.debug('Called FaceRecognition.getFacesCount() with filePath=%s', filePath)
        return len(self.getFaceEncodings(filePath))

    "Get an ObjectId of known person which is on the given image"
    def identifyPerson(self, filePath, knownPeople):
        logging.debug('Called FaceRecognition.identifyPerson() with filePath=%s and len(knownPeople)=%d', filePath, len(knownPeople))
        knownFaces = list(map(lambda o: numpy.frombuffer(o['faceEncoding']), knownPeople))

        faceEncodings = self.getFaceEncodings(filePath)

        if len(faceEncodings) == 1:
            recognitionResult = face_recognition.api.compare_faces(knownFaces, faceEncodings[0], 0.6)

            try:
                recognizedPersonIndex = recognitionResult.index(True)
                return knownPeople[recognizedPersonIndex]['_id']
            except ValueError:
                return None
        else:
            raise Exception('Image must contain only one face.')